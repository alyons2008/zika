
BEGIN;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;
CREATE EXTENSION IF NOT EXISTS unaccent;


COPY location (ID_0,ISO,NAME_0,ID_1,NAME_1,HASC_1,CCN_1,CCA_1,TYPE_1,ENGTYPE_1,NL_NAME_1,VARNAME_1,geom) FROM '/tmp/locations.csv';
COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit) FROM '/tmp/all_reports.csv' DELIMITER ',' CSV HEADER;

UPDATE location SET name_0_normalized = unaccent(name_0);
UPDATE location SET name_1_normalized = unaccent(name_1);


--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_lat_long) from '/tmp/Brazil_Zika-2016-04-02.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_lat_long) from '/tmp/Haiti_Zika-2016-02-03.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_lat_long) from '/tmp/Mexico_Zika-2016-02-20.csv' DELIMITER ',' CSV HEADER;
--COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit, location_lat_long) from '/tmp/Panama_Zika-2016-02-18.csv' DELIMITER ',' CSV HEADER;

COMMIT;

