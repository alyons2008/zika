package org.launchcode.zika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zika.data.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRepositoryTest {

    @Before
    public void setup(){
        reportRepository.deleteAllInBatch();
    }

    @After
    public void tearDown(){
        reportRepository.deleteAllInBatch();
    }

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private EntityManager entityManager;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetIndex() throws Exception {
        this.mockMvc.perform(get("/")).andExpect(status().isOk());
    }
//    @Test
//    public void findBySrcReturnsMatchingSrc() {
//        Report report = new Report("2017-03-01","United_States-Puerto_Rico","United States", "Puerto Rico","territory", "zika_confirmed_cumulative_2016","PR0003","NA","NA",38811.0,"cases");
//        entityManager.persist(report);
//        entityManager.flush();
//
//        List<Report> foundReport = reportRepository.findByReportDateContaining(report.getReportDate());
//
//        assertEquals(1, foundReport.size());
//        return;
//
//    }
}
