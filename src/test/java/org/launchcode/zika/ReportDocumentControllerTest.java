package org.launchcode.zika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zika.data.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportDocumentControllerTest extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

    @Before
    public void setup(){
        reportRepository.deleteAllInBatch();
    }

    @After
    public void tearDown(){
        reportRepository.deleteAllInBatch();
    }

    @Test
    public void emptyTest() throws Exception {

    }


//    @Test
//    public void testFuzzySearch() throws Exception {
//        Report testReport = new Report("2017-03-01","United_States-Puerto_Rico","United States", "Puerto Rico","territory", "zika_confirmed_cumulative_2016","PR0003","NA","NA",38811.0,"cases");
//        String json = json(testReport);
//        reportRepository.save(testReport);
////        mockMvc.perform(post("/api/es/report/")
////                .content(json)
////                .contentType(contentType));
//        mockMvc.perform(get("/api/es/report?search={term}", "United_States-Puerto_Rico"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(contentType))
//                .andExpect(jsonPath("$.length()").value(1))
//                .andExpect(jsonPath("$[0].reportDate").value(testReport.getReportDate()));
//    }

}