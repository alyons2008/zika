package org.launchcode.zika;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.zika.data.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ReportRestControllerTest extends AbstractBaseRestIntegrationTest{

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

//    @Before
//    public void createTestItems() {
//        Report report1 = new Report("2017-03-01","United_States-Puerto_Rico","United States", "Puerto Rico","territory", "zika_confirmed_cumulative_2016","PR0003","NA","NA",38811.0,"cases");
//        reportRepository.save(report1);
//
//        Report report2 = new Report("2017-03-01","United_States-Puerto_Rico","United States", "Puerto Rico","territory", "zika_confirmed_cumulative_2016","PR0003","NA","NA",38811.0,"cases");
//        reportRepository.save(report2);
//    return;
//    }

//    @Test
//    public void testGetSingleReport() throws Exception {
//        Report report = reportRepository.findAll().get(0);
//        mockMvc.perform(get("/api/es/report?reportDate={reportDate}", report.getReportDate()))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(contentType))
//                .andExpect(jsonPath(("[0].reportDate"), containsString("2017-03-01")));
//    }

    @Test
    public void testGetNotFoundReport() throws Exception {
        mockMvc.perform(get("/api/report/-1"))
                .andExpect(status().isNotFound());
    }
}
