
BEGIN;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;

COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,value,unit) FROM '/tmp/all_reports-cleaned.csv' DELIMITER ',' CSV HEADER;
COPY location(NAME_0,NAME_1,MULTI_POLYGON) FROM '/tmp/locations-cleaned.csv' DELIMITER ',' CSV HEADER;

UPDATE report SET location = replace(location, 'Pama', 'Panama');
UPDATE report SET location = replace(location, 'Argenti', 'Argentina');
UPDATE report SET value = 0 WHERE value IS null;

DELETE FROM location a USING location b WHERE a.id < b.id AND a.multi_polygon = b.multi_polygon;

COMMIT;

