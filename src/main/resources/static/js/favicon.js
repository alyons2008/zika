 function changeFavicon(src) {
     let link = document.createElement('link'),
         oldLink = document.getElementById('dynamic-favicon');
     link.id = 'dynamic-favicon';
     link.rel = 'shortcut icon';
     link.href = src;
     if (oldLink) {
      document.head.removeChild(oldLink);
     }
     document.head.appendChild(link);
}

    const btn = document.getElementById("sidebarCollapse");
    let changed = true;
    btn.onclick = function() {
     if(!changed){
        changeFavicon('favicon/mosquito.png');
        changed = true;
     }
     else{
        changeFavicon('favicon/noskeet.ico');
        changed = false;
     }
    };