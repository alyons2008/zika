/* global document,$,ol,window */

$(document).ready(() => {

    let dates = ['Dates not loaded.'];
    let states = ['No states yet'];
    let locations = ['Locations not loaded.'];
    let dataFields = ['Data Fields not loaded.'];
    let dateSelected = dates[0];
    let dateSliderIndex = 0;
    let reportFilter = [];
    let running = false;
    let playIndex = 0;

    /** Begin search block**/
    let searchButton = document.getElementById("searchButton");
    let searchText = "";

    searchButton.onclick = function elasticSearch() {
        filterChange();
    };
    /** End search block**/

    const updateDateDom = (index) => {
        dateSelected = dates[index];
        document.getElementById('date').value = index;
        document.getElementById('date-selected').innerHTML = dateSelected;
        document.getElementById('search-text').innerHTML = searchText;
    };

    const filterChange = (index) => {

        if (index < dates.length) {
            updateDateDom(index);
            reportFilter = [];

            var promise = $.getJSON('/api/es/report?reportDate=' + dateSelected, (data) => {
                data.features.forEach(function(d) {
                        reportFilter.push(d);
                    });
            });

            promise.then(() => {
                locationSource.refresh();
            });
        } else if (document.getElementById("date-switch").checked == false && document.getElementById("search-switch").checked == true) {
            console.log("false true");
            searchText = document.getElementById("search-text");
            reportFilter = [];
            dateSelected = null

            var promise = $.getJSON('/api/es/report?search=' + searchText.value, (data) => {
                data.features.forEach(function(d) {
                        reportFilter.push(d);
                    });
            });

            promise.then(() => {
                console.log("refresh");
                locationSource.refresh();
            });
        } else if (document.getElementById("date-switch").checked == true && document.getElementById("search-switch").checked == true) {
            console.log("true true");
            searchText = document.getElementById("search-text");
            reportFilter = [];

            var promise = $.getJSON('/api/es/report?reportDate=' + dateSelected + '&search=' + searchText.value, (data) => {
                data.features.forEach(function(d) {
                        reportFilter.push(d);
                    });
            });

            promise.then(() => {
                locationSource.refresh();
            });

        } else {
            updateDateDom(0);
        }
    };

    function rollDates() {
        if (playIndex <= dates.length) {
            if (playIndex < dates.length) {
                playIndex += 1;
            } else {
                playIndex = 0;
            }
            filterChange(playIndex);
        } else {
            playIndex = 0;
        }
    }

    function sliderEvent() {
            playIndex = parseInt(document.getElementById('date').value, 0);
            filterChange(playIndex);
    }


    function populateSlider() {
        dates = [];
        const promise = $.get('/api/report/dates', data => data.forEach(d => dates.push(d)));
        promise.then(() => {
               document.getElementById('date').setAttribute('max', dates.length - 1);
               dateSliderIndex = parseInt(document.getElementById('date').value, 0);
               filterChange(dateSliderIndex);
               document.getElementById('date').addEventListener('change', sliderEvent);
           });
    }

 /** Begin popup to display event information**/
    const container = document.getElementById('popup');
    const content = document.getElementById('popup-content');
    const closer = document.getElementById('popup-closer');


    const overlay = new ol.Overlay({
      element: container,
      autoPan: true,
      autoPanAnimation: {
        duration: 250
      }
    });

    /**
     * Add a click handler to hide the popup.
     * @return {boolean} Don't follow the href.
     */
    closer.onclick = function() {
      overlay.setPosition(undefined);
      closer.blur();
      return false;
    };

    window.onclick = function() {
      if (event.target == overlay) {
        overlay.style.display = "none";
      }
    }

/** End popup to display event information**/

    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true
    });

    // Define Location feature
    const locationSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: '/api/location/',
        overlaps: false,
    });

    function getStates() {
        states = [];
        $.get('/api/location/states', data => data.forEach(d => states.push(d)));
        return states;
    }

    // Define location style
    const locationStyle = (feature) => {
        let found = false;
        let totalValue = 0;
        let fillColor = 'rgba(0,0,0,0)';
        let strokeColor = 'rgba(0,0,0,0)';
        let strokeWidth = 0;

        for (let i = 0; i < reportFilter.length; i += 1) {

            if(states.includes(reportFilter[i].properties.state)){

                for (let i = 0, len = reportFilter.length; i < len; i += 1) {

                    if (reportFilter[i].properties.state === feature.get('state')) {
                        totalValue += reportFilter[i].properties.value;
                        found = true;
                    }
                }

                if(found){
                    if (totalValue < 20) {
                        fillColor = 'rgba(0,179,0,.05)';
                        strokeColor = 'green';
                        strokeWidth = 1;
                    } else if (totalValue < 790) {
                        fillColor = 'rgba(255,165,0,.1)';
                        strokeColor = 'orange';
                        strokeWidth = 1;
                    } else if (totalValue >= 790) {
                        fillColor = 'rgba(255,0,0,.05)';
                        strokeColor = 'red';
                        strokeWidth = 1;
                    }

                    return new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: strokeColor,
                            width: strokeWidth,
                        }),
                        fill: new ol.style.Fill({ color: fillColor }),
                    });
                }
            }
        }
    };

     const locationLayer = new ol.layer.Vector({
            source: locationSource,
            style: locationStyle,
        });

    const map = new ol.Map({
            controls: [
                //Define the default controls
                new ol.control.Zoom(),
                new ol.control.Attribution(),
                //Define some new controls
                new ol.control.ZoomSlider(),
                new ol.control.MousePosition({
                projection: 'EPSG:4326',
                coordinateFormat: function (coordinate) {
                        return 'Coordinates: ' + 'Lat ' + ol.coordinate.format(coordinate, '{y}', 3) + ' Long ' + ol.coordinate.format(coordinate, '{x}', 3);
                    },
                    target: 'coordinates'
                }),
                new ol.control.OverviewMap(),
                new ol.control.FullScreen()
            ],
            target: 'map',
            overlays: [overlay],
            layers: [
                osmLayer
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([-73.75,0]),
                zoom: 4
            })
    });

    map.addLayer(locationLayer);

/** Begin modal to display event information**/
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var modalContent = document.getElementById('modal-content');
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
/** End modal to display event information**/

    map.on('pointermove', function (event) {
        var hit = this.forEachFeatureAtPixel(event.pixel, function(feature, layer) {
            const coordinate = event.coordinate;
            content.innerHTML = `<h4><strong>OUTBREAK IN:</strong></h4><br>
                                 <p>${feature.get('country')}, ${feature.get('state')}
                                 <br>Reported on: ${reportFilter[0].properties.reportDate}
                                 </p>`
            overlay.setPosition(coordinate);
            return true;
        });
        if (hit) {
            this.getTargetElement().style.cursor = 'pointer';
        } else {
            this.getTargetElement().style.cursor = '';
            overlay.setPosition(undefined);
            closer.blur();
        }
    });

     map.on('click', (event) => {
       // foreach loop for each feature at pixel
       map.forEachFeatureAtPixel(event.pixel, (feature, layer) => {
         // get location from feature
         let country = feature.get('country');
         let state = feature.get('state');
         let location = country + '-' + state;

         // get a specific report from elasticsearch
         $.getJSON('/api/es/report?' + location + '&reportDate=' + dateSelected, {})
           .done(function (json) {
             // on completion from getJSON check for features
             if (json.features) {
               let reportTexts = [];
               let loc = "";
               // loop through features
               for(let i = 0; i < json.features.length; i++){
                   // save the information of the features to variables
                    if(json.features[i].properties.state == feature.get('state')){

                       loc = json.features[i].properties.location;
                       let dataField = json.features[i].properties.dataField;
                       let valueReported = json.features[i].properties.value;
                       let unit = json.features[i].properties.unit;

                       reportTexts.push(`<li>${dataField} - ${valueReported} ${unit}</li>`);

                    }
                    modal.style.display = "block";
               }

               // add report information to modal with a template string
              modalContent.innerHTML = `<h4>${loc}</h4><br>
                                        <ul>${reportTexts.join("")}</ul>`
             }

           });
       });
     });

    $('#date-switch').change(function () {
        $('.ol-control-slider').attr('disabled',! this.checked)
        $('#date-selected').hide();

        if (!this.checked && document.getElementById("search-switch").checked == false){
            locationLayer.setVisible(false);
            document.getElementById('date-selected').value = "";

        } else if (!this.checked) {
            $('#date-selected').hide();
        } else{
            $('#date-selected').show();
            locationLayer.setVisible(true);
        }
    });

    $('#search-switch').change(function () {
        $('#searchButton').attr('disabled',! this.checked)
        $('#search-text').attr('disabled',! this.checked)

        if (!this.checked && document.getElementById("date-switch").checked == false){
            locationLayer.setVisible(false);
            document.getElementById('search-text').value = "";
        } else{
            locationLayer.setVisible(true);
        }
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        setTimeout(function(){
              window.dispatchEvent(new Event('resize'));
        }, 250);
    });
    getStates();
    populateSlider();
});
