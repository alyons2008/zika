package org.launchcode.zika.util;

import org.launchcode.zika.data.ReportDocumentRepository;
import org.launchcode.zika.data.ReportRepository;
import org.launchcode.zika.models.Report;
import org.launchcode.zika.models.es.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EsUtil {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    public void reindex() {
        reportDocumentRepository.deleteAll();
        List<ReportDocument> reportDocuments = new ArrayList<>();
        for(Report report : reportRepository.findAll()) {
            reportDocuments.add(new ReportDocument(report));
        }
        reportDocumentRepository.saveAll(reportDocuments);
    }

    public void delete() {
        reportDocumentRepository.deleteAll();
    }
}