package org.launchcode.zika.data;

import org.launchcode.zika.models.es.ReportDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportDocumentRepository extends ElasticsearchRepository<ReportDocument, String> {

    @Query("{\"bool\": {\"must\": [{\"match\": {\"reportDate\": \"?0\"}}]}}")
    Iterable<ReportDocument> manualSearchOnDate(String reportDate);

    @Query("{\"multi_match\": {\"query\": \"?0\", \"fields\": [\"countryName\", \"stateName\"], \"fuzziness\": \"AUTO\"}}")
    Page<ReportDocument> manualFuzzySearchOnLocationStringPageable(String search, Pageable pageable);

    @Query("{\"bool\": {\"must\": [{\"match\": {\"reportDate\": \"?0\"}},{\"fuzzy\": {\"location\": \"?1\"}}]}}")
    Page<ReportDocument> manualFuzzySearchOnLocationStringMustMatchDatePageable(String reportDate, String search, Pageable pageable);

//    @Query("{\"query\": { \"filtered\": { \"query\" : { \"multi_match\": { \"query\": \"?0\", \"fields\": [\"countryName\",\"stateName\"]}},\"filter\": { \"bool\": { \"should\": { \"term\": { \"reportDate\": \"?1\" }}}}}}")
//    Page<ReportDocument> manualFuzzySearchOnLocationStringMustMatchDatePageable(String search, String reportDate, Pageable pageable);

    @Query("{\"bool\": {\"must\": [{\"match\": {\"reportDate\": \"?0\"}},{\"range\": {\"value\": {\"gte\": \"0\"}}}]}}")
    Page<ReportDocument> manualSearchOnDatePageable(String reportDate, Pageable pageable);

}