package org.launchcode.zika.data;

import org.launchcode.zika.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {

    @Query("SELECT DISTINCT reportDate FROM Report WHERE value > 0 ORDER BY reportDate")
    List<String> findDistinctReportDate();

    @Query("SELECT DISTINCT state FROM Report")
    List<String> findDistinctState();

    List<Report> findByLocationStartingWithIgnoreCase(String location);

    List<Report> findByReportDateContaining(String reportDate);

    List<Report> findByReportDateAndLocation(String reportDate, String location);
}


