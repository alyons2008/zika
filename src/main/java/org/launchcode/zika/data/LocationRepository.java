package org.launchcode.zika.data;

import org.launchcode.zika.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface LocationRepository extends JpaRepository<Location, Integer> {

    List<Location> findByCountryNameAndStateName(String country, String state);

    @Query("SELECT DISTINCT stateName FROM Location")
    List<String> findDistinctState();
}



