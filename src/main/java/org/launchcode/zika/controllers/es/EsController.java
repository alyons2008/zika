package org.launchcode.zika.controllers.es;

import org.launchcode.zika.util.EsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/es")
public class EsController {

    @Autowired
    private EsUtil esUtil;

    @RequestMapping(value = "/reindex", method = RequestMethod.GET)
    public ResponseEntity<String> reindex_get() {
        esUtil.reindex();
        return new ResponseEntity<>("Health", HttpStatus.OK);
    }

    @RequestMapping(value = "/reindex", method = RequestMethod.POST)
    public ResponseEntity<String> reindex() {
        esUtil.reindex();
        return new ResponseEntity<>("Health", HttpStatus.OK);
    }


    @RequestMapping(value = "/redelete", method = RequestMethod.POST)
    public ResponseEntity delete() {
        esUtil.delete();
        return new ResponseEntity("Refreshed Elasticsearch index\n", HttpStatus.OK);

    }
}