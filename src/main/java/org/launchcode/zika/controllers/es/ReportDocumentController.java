package org.launchcode.zika.controllers.es;

import org.launchcode.zika.data.LocationRepository;
import org.launchcode.zika.data.ReportDocumentRepository;
import org.launchcode.zika.features.Feature;
import org.launchcode.zika.features.FeatureCollection;
import org.launchcode.zika.models.Location;
import org.launchcode.zika.models.es.ReportDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/es/report")
public class ReportDocumentController {

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private LocationRepository locationRepository;

    @GetMapping
    public ResponseEntity<FeatureCollection> getReports(@RequestParam(name = "reportDate") Optional<String> reportDate,
                                                         @RequestParam(name = "search") Optional<String> search) {
        int count = 0;
        FeatureCollection featureCollection = new FeatureCollection();
        Page<ReportDocument> pages = null;
        Iterator<ReportDocument> iterator;

        if (reportDate.isPresent() && !search.isPresent()) {
            pages = reportDocumentRepository.manualSearchOnDatePageable(reportDate.get(), PageRequest.of(0, 250000));
        } else if (reportDate.isPresent() && search.isPresent()) {
            System.out.println("Search = " + search.get());
            pages = reportDocumentRepository.manualFuzzySearchOnLocationStringMustMatchDatePageable(reportDate.get(), search.get(), PageRequest.of(0, 250000));
        } else if(search.isPresent() && !reportDate.isPresent()){
            pages = reportDocumentRepository.manualFuzzySearchOnLocationStringPageable(search.get(), PageRequest.of(0, 250000));
        } else {
            pages = reportDocumentRepository.findAll(PageRequest.of(0, 250000));
        }
        if(!pages.hasContent()) {
            return new ResponseEntity<>(featureCollection, HttpStatus.OK);
        }
        for(ReportDocument reportDocument : pages) {


            count++;
            System.out.println(count);
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("id", reportDocument.getId());
            properties.put("reportDate", reportDocument.getReportDate());
            properties.put("value", reportDocument.getValue());
            properties.put("location", reportDocument.getLocation());
            properties.put("unit", reportDocument.getUnit());
            properties.put("dataField", reportDocument.getDataField());

            String state = "";
            String[] parts = reportDocument.getLocation().split("-");
            String country = parts[0];
            if (country.contains("_")) {
                country = country.replaceAll("_", " ");
            }
            if (parts.length < 2) {
                state = "";
            } else {
                state = parts[1];
                if (state.contains("_")) {
                    state = state.replaceAll("_", " ");
                }
            }

            List<Location> locations = locationRepository.findByCountryNameAndStateName(country, state);

            if(locations.size() > 0 && reportDocument.getLocation() != null) {
                properties.put("country", country);
                properties.put("state", state);
                featureCollection.addFeature(new Feature(locations.get(0).getGeom(), properties));
            }
        }
        return new ResponseEntity<>(featureCollection, HttpStatus.OK);
    }
}
