package org.launchcode.zika.controllers;

import org.launchcode.zika.data.LocationRepository;
import org.launchcode.zika.features.Feature;
import org.launchcode.zika.features.FeatureCollection;
import org.launchcode.zika.models.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/location")
public class LocationController {

    @Autowired
    LocationRepository locationRepository;

    @RequestMapping(value = "/")
    @ResponseBody
    public FeatureCollection getLocation(){
        List<Location> locations = locationRepository.findAll();

        if (locations.isEmpty()) {
            return new FeatureCollection();
        }

        FeatureCollection features = new FeatureCollection();

        for (Location location : locations) {
            features.addFeature(new Feature(location.getGeom(), createPropertiesFromLocation(location)));
        }

        return features;
    }

    private Map<String,Object> createPropertiesFromLocation(Location location) {
        Map<String, Object> properties = new HashMap<>();

        properties.put("country", location.getCountryName());
        properties.put("state", location.getStateName());

        return properties;
    }

}

