package org.launchcode.zika.controllers.rest;

import org.launchcode.zika.data.LocationRepository;
import org.launchcode.zika.data.ReportDocumentRepository;
import org.launchcode.zika.data.ReportRepository;
import org.launchcode.zika.models.Location;
import org.launchcode.zika.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/report")
public class ReportRestController {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @PostMapping(value = "/assignStates")
    public ResponseEntity<String> assignStates() {

        List<Location> locations = locationRepository.findAll();

        for (Location location : locations) {
            String countryPart = location.getCountryName().replace(" ", "_");
            String statePart = location.getStateName().replace(" ", "_");
            List<Report> matchingReports = reportRepository.findByLocationStartingWithIgnoreCase(countryPart + "-" + statePart);
            for (Report report : matchingReports) {
                report.setState(location);
            }
            reportRepository.saveAll(matchingReports);
        }

        return new ResponseEntity<>("Complete", HttpStatus.OK);
    }

    @GetMapping("/dates")
    public List<String> getReportDates(){
        return reportRepository.findDistinctReportDate();
    }
}
